import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class PositionRulesTest {
    @Test fun `evaluate twice to allow movement on position 19`() {
        val rule = PositionRules(19)
        assertTrue(rule.isRestrictionInPlace())
        assertFalse(rule.isRestrictionInPlace())
    }

    @Test fun `restriction can be forcefully removed`() {
        val rule = PositionRules(19)
        rule.removeRestriction()
        assertFalse(rule.isRestrictionInPlace())
    }

    @Test fun `restriction can also require helping hand`() {
        val rule = PositionRules(31)
        assertTrue(rule.isRestrictionInPlace())
        assertTrue(rule.requiresHelpingHand())
    }

    @Test fun `forcefully removing restrictions also removes helping hand`() {
        val rule = PositionRules(31)
        rule.removeRestriction()
        assertFalse(rule.isRestrictionInPlace())
    }

    @Test fun `moving beyond END moves back to 53 and sets 2 the restriction`() {
        val rule = PositionRulesWithDeathAndEndConditions(99)
        assertEquals(53, rule.position)
        assertTrue(rule.isRestrictionInPlace())
        assertTrue(rule.isRestrictionInPlace())
        assertFalse(rule.isRestrictionInPlace())
    }

    @Test fun `check new rule engine does not break old one 19`() {
        val rule = PositionRulesWithDeathAndEndConditions(19)
        assertTrue(rule.isRestrictionInPlace())
        assertFalse(rule.isRestrictionInPlace())
    }
}