import kotlin.test.*

class BoardTest {

    private lateinit var board: Board

    private fun givenPlayerOnPosition(name: String = "Agustin", position: Int) {
        board.addPlayer(name)
        board.movePlayerToPosition(name, position)
    }

    private fun givenTwoPlayersOnAGame() {
        board.addPlayer("Agustin")
        board.addPlayer("Malvina")
        board.movePlayerToPosition("Agustin", 1)
    }

    private fun givenThreePlayersWithTwoAlreadyMoved() {
        board.addPlayer("Agustin")
        board.addPlayer("Malvina")
        board.addPlayer("Player1")
        board.movePlayerToPosition("Player1", 1)
        board.movePlayerToPosition("Malvina", 1)
    }

    private fun givenTwoPlayersWithOneInHotel() {
        board.addPlayer("Agustin")
        board.addPlayer("Malvina")
        board.movePlayerToPosition("Agustin", 19)
        board.movePlayerToPosition("Malvina", 1)
    }

    private fun givenTwoPlayersWithOneInWell() {
        board.addPlayer("Agustin")
        board.addPlayer("Malvina")
        board.movePlayerToPosition("Agustin", 31)
        board.movePlayerToPosition("Malvina", 1)
    }

    @BeforeTest fun createBoard() {
        board = Board()
    }

    @Test fun `board can be created`() {
        Board()
    }

    @Test fun `can add players to board`() {
        board.addPlayer("Agustin")
    }

    @Test fun `can move player to position`() {
        givenPlayerOnPosition(position = 1)
    }

    @Test fun `get player current position`() {
        board.addPlayer("Agustin")
        assertEquals(0, board.currentPlayerPosition("Agustin"))
    }

    @Test fun `player position is updated`() {
        givenPlayerOnPosition(position = 5)
        assertEquals(5, board.currentPlayerPosition("Agustin"))
    }

    @Test fun `cannot move player if it has not been added`() {
        assertFailsWith<Board.NotAPlayerError> { board.movePlayerToPosition("Agustin", 3) }
    }

    @Test fun `player on special position gets position updated`() {
        givenPlayerOnPosition(position = 12)
        assertEquals(14, board.currentPlayerPosition("Agustin"))
    }

    @Test fun `player on bridge gets to proper new position`() {
        givenPlayerOnPosition(position = 6)
        assertEquals(12, board.currentPlayerPosition("Agustin"))
    }
    
    @Test fun `player cannot move to invalid position`() {
        givenPlayerOnPosition(position = 1)
        assertFailsWith<Board.InvalidPositionError> { board.movePlayerToPosition("Agustin", -1) }
    }

    @Test fun `non existent player attempts to get position fails`() {
        assertFailsWith<Board.NotAPlayerError> { board.currentPlayerPosition("Agustin") }
    }

    @Test fun `player cannot move if it is not his turn`() {
        givenTwoPlayersOnAGame()
        assertFailsWith<Board.InvalidTurnError> { board.movePlayerToPosition("Agustin", 5) }
    }

    @Test fun `Player1 needs to wait until every player has moved`() {
        givenThreePlayersWithTwoAlreadyMoved()
        assertFailsWith<Board.InvalidTurnError> { board.movePlayerToPosition("Player1", 2) }
    }

    @Test fun `player moves back when entering the maze`() {
        givenPlayerOnPosition(position = 42)
        assertEquals(39, board.currentPlayerPosition("Agustin"))
    }

    @Test fun `player misses a turn when on hotel`() {
        givenTwoPlayersWithOneInHotel()
        assertFailsWith<Board.InvalidTurnError> { board.movePlayerToPosition("Agustin", 20) }
    }

    @Test fun `player can move after staying on the hotel`() {
        givenTwoPlayersWithOneInHotel()
        board.movePlayerToPosition("Malvina", 2)
        board.movePlayerToPosition("Agustin", 20)
        assertEquals(20, board.currentPlayerPosition("Agustin"))
    }

    @Test fun `player cannot move when inside well`() {
        givenTwoPlayersWithOneInWell()
        board.movePlayerToPosition("Malvina", 2)
        assertFailsWith<Board.InvalidTurnError> { board.movePlayerToPosition("Agustin", 32) }
    }

    @Test fun `player can move after another takes his place`() {
        givenTwoPlayersWithOneInWell()
        board.movePlayerToPosition("Malvina", 31)
        board.movePlayerToPosition("Agustin", 32)
        assertEquals(32, board.currentPlayerPosition("Agustin"))
    }

    @Test fun `player on death position is moved back to starting position`() {
        board.addPlayer("Agustin", PositionRulesWithDeathAndEndConditions(0))
        board.movePlayerToPosition("Agustin", 58)
        assertEquals(0, board.currentPlayerPosition("Agustin"))
    }
}