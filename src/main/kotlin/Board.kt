import java.lang.Exception


class Board {
    class InvalidPositionError : Exception()
    class NotAPlayerError : Exception()
    class InvalidTurnError : Exception()

    private var playersPositions: MutableMap<String, PositionRules> = mutableMapOf()
    private var playersAllowedToMove: List<String> = emptyList()

    fun addPlayer(name: String, positionRules: PositionRules = PositionRules(0)) {
        playersPositions[name] = positionRules
    }

    fun movePlayerToPosition(name: String, position: Int) {
        checkPlayerExistsOnBoard(name)
        validatePosition(position)
        validatePlayerCanMove(name)
        evaluatePositionSideEffect(name, position)
    }

    fun currentPlayerPosition(name: String): Int {
        checkPlayerExistsOnBoard(name)
        return playersPositions[name]!!.position
    }

    private fun checkPlayerExistsOnBoard(name: String) {
        if (name !in playersPositions)
            throw NotAPlayerError()
    }

    private fun validatePosition(position: Int) {
        if (position < 1)
            throw InvalidPositionError()
    }

    private fun validatePlayerCanMove(name: String) {
        populatePlayersAllowedToMove()
        if (name !in playersAllowedToMove)
            throw InvalidTurnError()
        removePlayerUntilNextTurn(name)
    }

    private fun populatePlayersAllowedToMove() {
        if (playersAllowedToMove.isEmpty())
            playersAllowedToMove = playersPositions.filter { !it.value.isRestrictionInPlace() }.keys.toList()
    }

    private fun removePlayerUntilNextTurn(name: String) {
        playersAllowedToMove = playersAllowedToMove.filter { it != name }
    }

    private fun evaluatePositionSideEffect(name: String, position: Int) {
        playersPositions[name]!!.changePosition(position)
        if (playersPositions[name]!!.requiresHelpingHand())
            removeHelpingHandRestrictionIfApplicable(name, position)
    }

    private fun removeHelpingHandRestrictionIfApplicable(name: String, position: Int) {
        playersPositions.forEach { (key, value) ->
            if (key != name && value.position == position && value.requiresHelpingHand())
                value.removeRestriction()
        }
    }
}
