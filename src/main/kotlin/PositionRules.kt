open class PositionRules(var position: Int) {
    internal var helpingHand: Boolean = false
    internal var restrictionEffect: Int = 0

    companion object {
        internal const val START: Int = 0
        internal const val BRIDGE: Int = 6
        internal const val MAZE: Int = 42
        internal const val HOTEL: Int = 19
        internal const val WELL: Int = 31
        internal val PRISON: IntRange = 50..55
    }

    init {
        changePosition(position)
    }

    fun isRestrictionInPlace(): Boolean {
        return restrictionEffect-- > 0 || requiresHelpingHand()
    }

    fun removeRestriction() {
        restrictionEffect = 0
        helpingHand = false
    }

    fun requiresHelpingHand(): Boolean {
        return helpingHand
    }

    fun changePosition(value: Int) {
        position = value
        updateRestriction()
        updateHelpingHand()
        updatePosition()
    }

    protected open fun updateHelpingHand() {
        helpingHand = when (position) {
            WELL -> true
            in PRISON -> true
            else -> false
        }
    }

    protected open fun updateRestriction() {
        restrictionEffect = when (position) {
            HOTEL -> 1
            else -> 0
        }
    }

    protected open fun updatePosition() {
        position += when {
            position == START -> 0
            position == BRIDGE -> 6
            position == MAZE -> -3
            position % 6 == 0 -> 2
            else -> 0
        }
    }
}
