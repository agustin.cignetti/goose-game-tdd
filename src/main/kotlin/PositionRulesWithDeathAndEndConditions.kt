class PositionRulesWithDeathAndEndConditions(position: Int) : PositionRules(position) {
    override fun updatePosition() {
        super.updatePosition()
        position += when {
            position == DEATH -> -58
            position > END -> END - (position + 10)
            else -> 0
        }
    }

    override fun updateRestriction() {
        super.updateRestriction()
        restrictionEffect = when {
            position > END -> 2
            else -> restrictionEffect
        }
    }

    companion object {
        internal const val DEATH: Int = 58
        internal const val END: Int = 63
    }
}